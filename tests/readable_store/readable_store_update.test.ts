import { WritableStore } from '../../src/store/writable_store.ts'
import { StoreOptions } from '../../src/store/store_options.ts'
import { assertEquals } from "https://deno.land/std@0.90.0/testing/asserts.ts";

const test = Deno.test

test({
    name: `Given a readable store associated to a writable store. 
           When the writable store has a value of 10 and update its own value by adding 20 to it.
           Then, the readable store should return 30 with the get() method.`,
    fn() {
        const writable_store = new WritableStore(10)
        const readable_store = writable_store.to_readable_store()

        writable_store.update(value => value + 20)
        
        assertEquals(readable_store.get(), 30)
    }
})

test({
    name: `Given a readable store associated to a writable store. 
           When the writable store has a value of 0 and update its own value by adding 10 to it, 10 times.
           Then, the readable store should return 100 with the get() method.`,
    fn() {
        const writable_store = new WritableStore(0)
        const readable_store = writable_store.to_readable_store()

        for (let i = 0; i < 10; i++) {
            writable_store.update(value => value + 10)
        }
        
        assertEquals(readable_store.get(), 100)
    }
})
