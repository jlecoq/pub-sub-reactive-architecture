import { WritableStore } from '../../src/store/writable_store.ts'
import { StoreOptions } from '../../src/store/store_options.ts'
import { assertEquals } from "https://deno.land/std@0.90.0/testing/asserts.ts";

const test = Deno.test

test({
    name: `Given a readable store associated to a writable store. 
           When the writable store set its own value to 10. 
           Then, the readable store should return 10 with the get() method.`,
    fn() {
        const writable_store = new WritableStore(0)
        const readable_store = writable_store.to_readable_store()
        const value = 10

        writable_store.set(value)
        
        assertEquals(readable_store.get(), value)
    }
})

test({
    name: `Given a readable store associated to a writable store. 
           When the writable store has a value of 0 and set its own value to 10, 10 times.
           Then, the readable store should return 10 with the get() method.`,
    fn() {
        const writable_store = new WritableStore(0)
        const readable_store = writable_store.to_readable_store()
        const value = 10

        for (let i = 0; i < 10; i++) {
            writable_store.set(value)
        }
        
        assertEquals(readable_store.get(), value)
    }
})
