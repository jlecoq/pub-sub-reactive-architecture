import { WritableStore } from '../../src/store/writable_store.ts'
import { StoreOptions } from '../../src/store/store_options.ts'
import { assertEquals } from "https://deno.land/std@0.90.0/testing/asserts.ts";
import { ParamCallback } from '../../src/types.ts'
import { Subscriber } from '../../src/store/subscriber.ts'

const test = Deno.test

test({
    name: `Given a subscriber that subscribe to a readable store with no subscribe time defined.
           When the store emits a given number of times.
           Then, the subscriber should be called the number of time the store has emitted.`,
    fn() {
        const count_writable = new WritableStore(0)
        let num_of_time_called = 0

        count_writable.subscribe((value: number) => {
            num_of_time_called++
        })

        const number_of_emits = 20

        for (let i = 0; i < number_of_emits; i++) {
            count_writable.set(10)
        }

        assertEquals(num_of_time_called, number_of_emits)
    }
})

test({
    name: `Given a subscriber that subscribe to a readable store with a subscribe time equals to 1.
           When the store emits 2 or more times.
           Then, the subscriber should only be called 1 time.`,
    fn() {
        let num_of_time_called = 0
        const count_writable = new WritableStore(0)
        const options = new StoreOptions(1)

        count_writable.subscribe((value: number) => {
            num_of_time_called++
        }, options)

        count_writable.set(10)
        count_writable.set(10)

        assertEquals(num_of_time_called, 1)

        count_writable.set(10)

        assertEquals(num_of_time_called, 1)
    }
})

