import { Subscriber } from './subscriber.ts'
import { StoreOptions } from './store_options.ts'
import { ParamReturnCallback, ParamCallback, Callback } from '../types.ts'
import { Readable } from './readable.ts'
import { ReadableStore } from './readable_store.ts'

export class WritableStore<T> implements Readable<T> {
    private subscribers: Map<Subscriber<T>, Subscriber<T>> = new Map()

    constructor(private value: T) { }

    private emit() {
        for (const [_, subscriber] of this.subscribers) {
            const { options } = subscriber
            subscriber.handler(this.value)

            if (options.subscribe_time != null) {
                options.subscribe_time--

                if (options.subscribe_time == 0) {
                    this.subscribers.delete(subscriber)
                }
            }
        }
    }

    set(value: T) {
        this.value = value
        this.emit()
    }

    get() {
        return this.value
    }

    update(emitter: ParamReturnCallback<T, T>) {
        this.value = emitter(this.value)
        this.emit()
    }

    subscribe(handler: ParamCallback<T>,
              options: StoreOptions = new StoreOptions()): Callback | null {
        if (options.get_retained_data) {
            handler(this.value)

            if (options.subscribe_time != null) {
                if (options.subscribe_time == 1) {
                    return null
                } else {
                    options.subscribe_time--
                }
            }
        }
 
        const subscriber = new Subscriber(handler, options)
        this.subscribers.set(subscriber, subscriber)

        return () => {
            this.subscribers.delete(subscriber)
        }
    }

    to_readable_store(): ReadableStore<T> {
        return new ReadableStore(this) 
    }
}
