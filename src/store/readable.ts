import { ParamCallback, Callback } from '../types.ts'
import { StoreOptions } from './store_options.ts'

export interface Readable<T> {
    get(): T
    subscribe(handler: ParamCallback<T>, options: StoreOptions): Callback | null
}
