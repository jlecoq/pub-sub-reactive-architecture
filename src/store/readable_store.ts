import { ParamReturnCallback, ParamCallback, Callback } from '../types.ts'
import { Subscriber } from './subscriber.ts'
import { StoreOptions } from './store_options.ts'
import { Readable } from './readable.ts'
import { WritableStore } from './writable_store.ts'

export class ReadableStore<T> implements Readable<T> { 
    constructor(private writable: WritableStore<T>) { }
    
    get(): T {
        return this.writable.get()
    }

    subscribe(handler: ParamCallback<T>,
              options: StoreOptions = new StoreOptions()): Callback | null {
        return this.writable.subscribe(handler, options)
    }
}
