export class StoreOptions {

    /**
     * A null subscribe_time means an infinite subscription time.
     */
    constructor(public subscribe_time: number | null = null,
                public get_retained_data = false) {
        if (subscribe_time != null && subscribe_time <= 0) {
            throw new Error("The subscribeTime should be greather than 0")
        }
    }
}
