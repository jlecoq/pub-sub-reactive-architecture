import { ParamCallback } from '../types.ts'
import { StoreOptions } from './store_options.ts'

export class Subscriber<T> {
    constructor(public handler: ParamCallback<T>,
                public options: StoreOptions) { } 
}
