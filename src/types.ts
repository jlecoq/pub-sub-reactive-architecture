export type ParamCallback<T> = (value: T) => void
export type ParamReturnCallback<T, K> = (value: T) => K
export type Callback = () => void
