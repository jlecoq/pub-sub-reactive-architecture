import { WritableStore } from '../src/store/writable_store.ts'
import { StoreOptions } from '../src/store/store_options.ts'

function log(value: number) {
    console.log(value)
}

function example1() {
    const counter = new WritableStore(0)

    counter.subscribe(log) 
    counter.subscribe(log)

    counter.set(10)
}

function example2() {
    const counter = new WritableStore(0)

    counter.subscribe(log) 
    counter.subscribe(log)

    counter.update(value => value += 10)
    counter.update(value => value += 10)
}

function example3() {
    const counter = new WritableStore(0)

    log(counter.get())

    counter.set(10)

    log(counter.get())
}

function example4() {
    const counter = new WritableStore(0)
    const options = new StoreOptions(1)

    counter.subscribe(log, options) 

    counter.set(10)
    counter.set(10)
}

function example5() {
    const counter = new WritableStore(0)
    const options = new StoreOptions(null, true)

    counter.subscribe(log, options) 
}


function example6() {
    const counter = new WritableStore(0)
    const readableCounter = counter.to_readable_store()

    readableCounter.subscribe(log)

    counter.set(10)
}

/*
 * You can convert a WritableStore to a ReadableStore in order to prevent someone to emit
 * data from the WritableStore. 
 * The person can still listen to the store emittions via the ReadableStore. The ReadableStore
 * all the same read methods of the WritableStore.
 *
 */

